'use strict';
function $(id){
    return document.getElementById(id);
}

function removeArrayElement(array, value){
    var index = array.indexOf(value);
    if (index > -1) {
        array.splice(index, 1);
    }
}
// INICIO tabla dinámica
const una_tabla = new Vue({
    el: '#una_tabla',
    data: {
      registros: [],
      tableHeadings: [{label: "Nombre", input_type: "text", name: "nombre"},
                      {label: "Edad", input_type: "number", name: "edad"}, 
                      {label: "Fecha", input_type: "date", name: "fecha"},
                      {label: "Elegido", input_type: "checkbox", name: "elegido"}],
      editando: null,
      agregando: null
    },
    methods: {
        beautifyData(data, type){
            //Cambia información a visualizar en cada elemento de la grilla por otros
            let dataBeautify;
            switch (type){
                case "checkbox":
                    dataBeautify = data ? "Si": "No";
                    break;
                case "date":
                    let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                    dataBeautify = new Date(data).toLocaleDateString("es-AR", options);
                    break;
                default:
                    dataBeautify = data;
                    break;
            }
            return dataBeautify;
        },
        Borrar(row) {
            removeArrayElement(this.registros, row);
        },        
        
        ModoEdicion(row){
            this.lastRow = Object.assign({}, row);
            this.editando = row.id;
        },
        CancelarEdicion(row){
            if (this.agregando!==null){
                removeArrayElement(this.registros, row);
                this.agregando = null;
            }else{
                Object.assign(row, this.lastRow);
                this.editando = null;
            }
        },
        Guardar(row) {
            //validaciones
            if (row.nombre === '' || row.fecha === null){
                alert("Debe diligenciar el nombre y la fecha");
                return;
            }
            //fin validaciones
            this.editando = null; //todo OK, desactiva vista para editar
            this.agregando = null;//todo OK, desactiva vista para añadir
        }
    }
});

function ModoAdicion_unatabla(){
    if (una_tabla.agregando !== null || una_tabla.editando !== null)
        return;

    una_tabla.agregando = una_tabla.registros.length;
    let registro = {
        nombre: null,
        fecha: null,
        edad: null,
        elegido: false,
        id: una_tabla.registros.length
    };
    una_tabla.registros.push(registro);
}
//FIN tabla dinámica


// INICIO tabla estática
const tabla_hija = new Vue({
    el: '#tabla_hija',
    data: {
      registros: [],
      editando: null,
      agregando: null
    },
    methods: {
        Borrar(row){
            removeArrayElement(this.registros, row);
        },        
        ModoEdicion(row) {
            this.lastRow = Object.assign({}, row);
            this.editando = row.id;
        },
        CancelarEdicion(row){
            if (this.agregando!==null){
                removeArrayElement(this.registros, row);
                this.agregando = null;
            }else{
                Object.assign(row, this.lastRow);
                this.editando = null;
            }
        },
        Guardar(row) {
            //validaciones
            if (row.detalle === '' || row.Componente === ''){
                alert("Debe diligenciar el detalle y el Componente");
                return;
            }
            //fin validaciones
            this.editando = null; //todo OK, desactiva vista para editar
            this.agregando = null;//todo OK, desactiva vista para añadir
        }
    }
});

function ModoAdicion_tabla_hija(){
    if (una_tabla.agregando !== null || una_tabla.editando !== null)
        return;

    tabla_hija.agregando = tabla_hija.registros.length;
    let registro = {
        detalle: null,
        fecha_detalle: null,
        Componente: null,
        Valor: null,
        telefono: null,
        id: tabla_hija.registros.length
    };
    tabla_hija.registros.push(registro);
}
//FIN tabla estática

function Enviar(){
    //verifica si hay un cambio pendiente 
    if ((tabla_hija.editando !== null || tabla_hija.agregando !== null) || (una_tabla.editando !== null || una_tabla.agregando !== null)){
        if (confirm("¿Esta seguro de guardar los cambios en las tablas?")== false){
            return; //Se sale ya que no va a guardar los cambios, aunque ya estan guardados en una_tabla.registros
        }else{
            //cambia a editando = null
            tabla_hija.Editar(tabla_hija.registros);
            una_tabla.Editar(una_tabla.registros);
        }
    }

    //Envia array al servidor restful
    let datosEnviarUnaTabla = JSON.stringify(una_tabla.registros);
    let datosEnviarTablaHija = JSON.stringify(tabla_hija.registros);
    alert(`Datos a enviar una Tabla: ${datosEnviarUnaTabla}
    |      Datos a enviar Tabla Hija: ${datosEnviarTablaHija}         
    `);

}